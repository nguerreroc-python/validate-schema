import json
from jsonschema import Draft7Validator

def validateJsonSchema():
    validationResult = {
        "isValid": True, "errors": ""
    }
    jsonSchema = getSchema('userFullSchema')
    jsonData = getJson('userFull')

    validator = Draft7Validator(jsonSchema)
    if not validator.is_valid(jsonData):
        errors = createErrors(validator.iter_errors(jsonData))
        #errors = ""
        validationResult = {
            "isValid": False, "errors": errors
        }

    return validationResult

def getSchema(filename):
    with open(f'./schemas/{filename}.json', 'r') as file:
        schema = json.load(file)
    return schema

def getJson(filename):
    with open(f'./docs/{filename}.json', 'r') as file:
        doc = json.load(file)
    return doc

def createErrors(iter_errors):
    errors = []
    for error in list(iter_errors):
        errors.append({
            "attribute": error.absolute_path[0],
            "message": error.message
        })

    return errors

# validate it
result = validateJsonSchema()
print(result)
